# basic_dff.py
# results is same as "basicDff.v" in the Verilog tutorial

from myhdl import *

@block
def basic_dff(clk, reset_n, d, q):
    """ clk, reset, d : input
        q : output
    """

    @always(clk.posedge, reset_n.negedge)
    def dff_behave():
        if reset_n == 0 :
            q.next = 0
        else :
            q.next = d
    return dff_behave


def main():
    clk = Signal(bool(0))
    reset_n = Signal(bool(0))
    d = Signal(bool(0))
    q = Signal(bool(0))

    top_dff = basic_dff(clk, reset_n, d, q)
    top_dff.convert(hdl="Verilog", initial_values=True)

# function main() is the entry point
if __name__ == '__main__':
    main()
