# clock_tick.py

from myhdl import *

from mod_m_counter import mod_m_counter

@block
def clock_tick(clk, reset_n, clk_pulse, M, N):
    # count = Signal(intbv(0)[N:0])
    # mod_m_counter_inst = mod_m_counter(clk, reset_n, clk_pulse, count, M, N)

    # None for count-port
    mod_m_counter_inst = mod_m_counter(clk, reset_n, clk_pulse, None, M, N)
    return mod_m_counter_inst

# instantiate clock_tick
def main():
    clk = Signal(bool(0))
    reset_n = Signal(bool(0))
    clk_pulse = Signal(bool(0))
    M = 5000000 # 0.1 sec
    N = 29

    clock_tick_inst = clock_tick(clk, reset_n, clk_pulse, M, N)
    clock_tick_inst.convert(hdl="Verilog", initial_values=True)

if __name__ == '__main__':
    main()
