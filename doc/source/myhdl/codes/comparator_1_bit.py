# comparator_1_bit.py

# the resultant Verilog code is same as Listing 'comparator1Bit.v 
# of Verilog tutorial

from myhdl import *

# 1-bit comparator using procedural assignment
@block
def comparator_1_bit_proc(x, y, eq):
    """ x, y : input
        eq : output

        eq = 1 when x = y else 0
    """

    s0 = Signal(bool(0))
    s1 = Signal(bool(0))

    @always(s0, s1)
    def comparator_1_bit_behave():
        s0.next = ~x & ~y
        s1.next = x & y
        eq.next = s0 | s1

    return comparator_1_bit_behave


# 1-bit comparator using continuous assignment
@block
def comparator_1_bit_comb(x, y, eq):
    """ x, y : input
        eq : output

        eq = 1 when x = y else 0
    """

    @always_comb
    def comparator_1_bit_behave():
        eq.next = (~x & ~y) | (x & y)

    return comparator_1_bit_behave

def main():
    x = Signal(bool(0)) # signal of type boolean size 1 bit
    y = Signal(bool(0))
    eq = Signal(bool(0))

    # convert into Verilog code
    comparator_verilog_procedure = comparator_1_bit_proc(x=x, y=y, 
                eq=eq).convert(hdl="Verilog", initial_values=True)
    comparator_verilog_continuous = comparator_1_bit_comb(x=x, y=y, 
                eq=eq).convert(hdl="Verilog", initial_values=True)

if __name__ == '__main__':
    main()
