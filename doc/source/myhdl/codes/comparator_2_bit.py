# comparator_2_bit.py
# the resultant Verilog code is same as Listing 'comparator2Bit.v 
# of Verilog tutorial

from myhdl import *

@block
def comparator_2_bit(x, y, eq):
    """ x, y : input
        eq : output

        eq = 1 when x = y else 0
    """

    # 'intbv' is used instead of 'bool' as 'bool' is not subscriptable
    s = Signal(intbv(0)[4:0])

    @always(s)
    def comparator_2_bit_behave():
        s[0].next =  ~x[1] & ~x[0] & ~y[1] & ~y[0]
        s[1].next =  ~x[1] & x[0] & ~y[1] & y[0]
        s[2].next =  x[1] & ~x[0] & y[1] & ~y[0]
        s[3].next =  x[1] & x[0] & y[1] & y[0]
        eq.next = s[0] | s[1] | s[2] | s[3]

    return comparator_2_bit_behave

def main():
    # 'intbv' is used instead of 'bool' as 'bool' is not subscriptable
    x = Signal(intbv(0)[2:0]) # signal of type boolean size 1-bit
    y = Signal(intbv(0)[2:0])
    eq = Signal(bool(0))

    # convert into Verilog code
    comparator_verilog = comparator_2_bit(x=x, y=y, eq=eq)
    comparator_verilog.convert(hdl="Verilog", initial_values=True)

if __name__ == '__main__':
    main()
