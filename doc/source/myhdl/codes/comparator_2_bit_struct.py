# comparator_2_bit_struct.py
# this code is similar to comparator2BitStruct.v of verilog tutorial

from myhdl import *
from comparator_1_bit import comparator_1_bit_proc

@block
def comparator_2_bit_struct(a, b, eq):
    # s = Signal(intbv(0)[2:0]) # this does not work
    s0 = Signal(bool(0))
    s1 = Signal(bool(0))
          
    # instantiation : 1-bit comparator
    # note square brackets are not used i.e. a(0) and b(0)
    eq0 = comparator_1_bit_proc(x=a(0), y=b(0), eq=s0) 
    eq1 = comparator_1_bit_proc(x=a(1), y=b(1), eq=s1)

    @always(s0, s1)
    def comparator_2_bit_behave():
        if ((s0 == 1) & (s1 == 1)) :
            eq.next = 1
        else : 
            eq.next = 0
    return comparator_2_bit_behave, eq0, eq1

def main():
    x = Signal(intbv(0)[2:0])
    y = Signal(intbv(0)[2:0])
    eq = Signal(bool(0))

    # convert into Verilog code
    comparator_verilog = comparator_2_bit_struct(a=x, b=y, eq=eq)
    comparator_verilog.convert(hdl="Verilog", initial_values=True)

if __name__ == '__main__' :
    main()
