# counter_ex.py
# results are same as the Listing 'counter_ex.v' in Verilog tutorial

from myhdl import *

@block
def counter_ex(clk, reset_n, out_moore, M, N):
    states = enum('start_moore', 'count_moore')
    state_moore_reg = Signal(states.start_moore)
    state_moore_next = Signal(states.start_moore)

    count_moore_reg = Signal(intbv(0)[N:0])
    count_moore_next = Signal(intbv(0)[N:0])

    @always(clk.posedge, reset_n.negedge)
    def current_state_logic():
        if reset_n == 0 :
            state_moore_reg.next = states.start_moore
            count_moore_reg.next = 0
        else :
            state_moore_reg.next = state_moore_next
            count_moore_reg.next = count_moore_next

    @always_comb
    def next_state_logic():
        if state_moore_reg == states.start_moore :
            count_moore_next.next = 0
            state_moore_next.next = states.count_moore
        elif state_moore_reg == states.count_moore :
            count_moore_next.next = count_moore_reg + 1
            if count_moore_reg + 1 == M - 1 :
                state_moore_next.next = states.start_moore
            else :
                state_moore_next.next = states.count_moore

    @always_comb
    def assign_output_logic():
        out_moore.next = count_moore_reg

    return current_state_logic, next_state_logic, assign_output_logic

def main():
    M = 6
    N = 4
    clk = Signal(bool(0))
    reset_n = Signal(bool(0))
    out_moore = Signal(bool(0))

    counter_ex_verilog = counter_ex(clk, reset_n, out_moore, M, N)
    counter_ex_verilog.convert(hdl="Verilog", initial_values=True)

if __name__ == '__main__':
    main()
