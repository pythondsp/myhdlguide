# d_ff.py
# results is same as "D_FF.v" in the Verilog tutorial

from myhdl import *

@block
def dff_unit(clk, reset_n, en, d, q):
    """ clk, reset, en, d : input
        q : output
    """

    @always(clk.posedge, reset_n.negedge)
    def dff_behave():
        if reset_n == 0 :
            q.next = 0
        # else :
            # q.next = d
        elif  en == 1 :
            q.next = d
    return dff_behave

# below module is created to change the name of the port
# i.e. clk is changed to CLOCK_50
@block
def d_ff(CLOCK_50, reset_n, en, d, q):
    dff_top = dff_unit(CLOCK_50, reset_n, en, d, q)
    return dff_top

def main():
    clk = Signal(bool(0))
    reset_n = Signal(bool(0))
    en = Signal(bool(0))
    d = Signal(bool(0))
    q = ResetSignal(0, active=0, async=True)

    top_dff2 = d_ff(clk, reset_n, en, d, q)
    top_dff2.convert(hdl="Verilog", initial_values=True)

# function main() is the entry point
if __name__ == '__main__':
    main()
