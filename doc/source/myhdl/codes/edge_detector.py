# edge_detector.py
# the results are same as "edgeDetector.v" in the Verilog tutorial

from myhdl import *

@block
def edge_detector(clk, reset_n, level, mealy_tick, moore_tick):

    # mealy states
    mealy_states = enum('zero_mealy', 'one_mealy')
    state_mealy_reg = Signal(mealy_states.zero_mealy)
    state_mealy_next = Signal(mealy_states.zero_mealy)

    # moore states
    moore_states = enum('zero_moore', 'edge_moore', 'one_moore')
    state_moore_reg = Signal(moore_states.zero_moore)
    state_moore_next = Signal(moore_states.zero_moore)

    # current state logic
    @always(clk.posedge, reset_n.negedge)
    def state_current_logic():
        if reset_n == 0 :
            state_moore_reg.next = moore_states.zero_moore
            state_mealy_reg.next = mealy_states.zero_mealy
        else :
            state_moore_reg.next = state_moore_next
            state_mealy_reg.next = state_mealy_next

    # next state logic : Mealy design
    @always_comb
    def state_next_logic_mealy():
        state_mealy_next.next = state_mealy_reg
        mealy_tick.next = 0
        if state_mealy_reg == mealy_states.zero_mealy :
            if level == 1 :
                state_mealy_next.next = mealy_states.one_mealy
                mealy_tick.next = 1
        elif state_mealy_reg == mealy_states.one_mealy:
            if level != 1 :
                state_mealy_next.next = mealy_states.zero_mealy

    # next state logic : Moore design
    @always_comb
    def state_next_logic_moore():

        state_moore_next.next = state_moore_reg
        moore_tick.next = 0
        if state_moore_reg == moore_states.zero_moore :
            if level == 1 :
                state_moore_next.next = moore_states.edge_moore
        elif state_moore_reg == moore_states.edge_moore:
            moore_tick.next = 1
            if level == 1 :
                state_moore_next.next = moore_states.one_moore
            else :
                state_moore_next.next = moore_states.zero_moore
        elif state_moore_reg == moore_states.one_moore:
            if level != 1 :
                state_moore_next.next = moore_states.zero_moore


    return state_current_logic, state_next_logic_moore, state_next_logic_mealy

def main():
    clk = Signal(bool(0))
    reset_n = Signal(bool(0))
    level = Signal(bool(0))
    moore_tick = Signal(bool(0))
    mealy_tick = Signal(bool(0))

    edge_detector_verilog = edge_detector(clk, reset_n, level, mealy_tick, moore_tick)
    edge_detector_verilog.convert(hdl="Verilog", initial_values=True)

if __name__ == '__main__':
    main()
