# gateEx.py
# the resultant Verilog code is same as Listing 'andEx.v of Verilog tutorial

from myhdl import *

# Module : and gate
@block
def andEx(x, y, z):
    """ input : x, y
        output : z
    """

    # behavior : and gate
    # i.e. implement and gate using combinational logic
    @always_comb
    def and_behave():
        z.next = x & y # and operation

    # return instances individually
    # return and_behave

    # this is more convenient as it returns all the instances automatically
    return instances()

# Module : xor gate
@block
def xorEx(x, y, z):
    """ input : x, y
        output : z
    """

    # behavior : xor gate
    # i.e. implement xor gate using combinational logic
    @always_comb
    def xor_behave():
        z.next = x ^ y # xor operation

    # return instances individually
    # return xor_behave

    # this is more convenient as it returns all the instances automatically
    return instances()


# top level entity for 'xor' gate
@block
def top_xorEx(SW, LEDG):
    # instantiate xorEx : display only output on green LED (LEDG)
    xorEx_verilog = xorEx(x=SW(0), y=SW(1), z=LEDG)

    return instances() 

def main():
    x = Signal(bool(0)) # signal of type boolean size 1-bit
    y = Signal(bool(0))
    z = Signal(bool(0))

    switch = Signal(intbv(0)[2:0]) # 2 switches
    led = Signal(intbv(0)[3:0]) # 3 green LED

    # convert into Verilog code
    andEx_verilog = andEx(x, y, z)

    # initial_values = True will initialize the signal 
    # note that there is no signal in this design (all are ports)
    andEx_verilog.convert(hdl="Verilog", initial_values=True)

    # convert into VHDL code  : convert without instantiation
    andEx(x, y, z).convert(hdl="VHDL", initial_values=True)


    # convert into Verilog code
    xorEx_verilog = xorEx(x, y, z)

    # initial_values = True will initialize the signal 
    # note that there is no signal in this design (all are ports)
    xorEx_verilog.convert(hdl="Verilog", initial_values=True)

    # convert into VHDL code  : convert without instantiation
    xorEx(x, y, z).convert(hdl="VHDL", initial_values=True)

    # modified port name according to pin-assignment file
    top_xorEx(switch, led).convert(hdl="Verilog", initial_values=True)

if __name__ == '__main__':
    main()
