# gateEx_convert.py
# convert the gateEx.py to VHDL/Verilog code
# the resultant Verilog code is same as Listing 'andEx.v of Verilog tutorial

from myhdl import *
from gateEx import andEx, xorEx

x = Signal(bool(0)) # signal of type boolean size 1-bit
y = Signal(bool(0))
z = Signal(bool(0))

# convert into Verilog code
andEx_verilog = andEx(x, y, z)

# initial_values = True will initialize the signal 
# note that there is no signal in this design (all are ports)
andEx_verilog.convert(hdl="Verilog", initial_values=True)

# convert into VHDL code  : convert without instantiation
andEx(x, y, z).convert(hdl="VHDL", initial_values=True)


# convert into Verilog code
xorEx_verilog = xorEx(x, y, z)

# initial_values = True will initialize the signal 
# note that there is no signal in this design (all are ports)
xorEx_verilog.convert(hdl="Verilog", initial_values=True)

# convert into VHDL code  : convert without instantiation
xorEx(x, y, z).convert(hdl="VHDL", initial_values=True)
