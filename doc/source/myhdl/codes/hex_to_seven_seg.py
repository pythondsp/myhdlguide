# hex_to_seven_seg.py
# result is same as 'hexToSevenSegment.v' and
# 'hexToSevenSegment_testCircuit.v' in the Verilog tutorial

from myhdl import *

@block
def hex_to_seven_seg(hex_num, seven_seg):

    @always(hex_num)
    def logic():
        if hex_num == 0:
            seven_seg.next = 0b1000000
        elif hex_num == 1 :
            seven_seg.next = 0b1111001
        elif hex_num == 2 :
            seven_seg.next = 0b0100100
        elif hex_num == 3 :
            seven_seg.next = 0b0110000
        elif hex_num == 4 :
            seven_seg.next = 0b0011001
        elif hex_num == 5 :
            seven_seg.next = 0b0010010
        elif hex_num == 6 :
            seven_seg.next = 0b0000010
        elif hex_num == 7 :
            seven_seg.next = 0b1111000
        elif hex_num == 8 :
            seven_seg.next = 0b0000000
        elif hex_num == 9 :
            seven_seg.next = 0b0010000
        elif hex_num == 10 :
            seven_seg.next = 0b0001000
        elif hex_num == 11 :
            seven_seg.next = 0b0000011
        elif hex_num == 12 :
            seven_seg.next = 0b1000110
        elif hex_num == 13 :
            seven_seg.next = 0b0100001
        elif hex_num == 14 :
            seven_seg.next = 0b0000110
        else :
        # elif hex_num == 15 :
            seven_seg.next = 0b0001110

    return logic

# instantiate hex_to_seven_seg with new port names
@block
def hex_to_seven_seg_test(SW, HEX0):
    hex_to_seven_seg_inst = hex_to_seven_seg(SW, HEX0)
    return hex_to_seven_seg_inst

# instantiate hex_to_seven_seg_test
def main():
    hex_num = Signal(intbv(0)[4:0])
    seven_seg = Signal(intbv(0)[7:0])

    hex_ss_v = hex_to_seven_seg_test(hex_num, seven_seg)
    hex_ss_v.convert(hdl="Verilog", initial_values=True)
    

if __name__ == '__main__':
    main()
