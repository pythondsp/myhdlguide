# mod_m_counter_visual_test.py

from myhdl import *

from mod_m_counter import mod_m_counter
from clock_tick import clock_tick
from hex_to_seven_seg import hex_to_seven_seg_test

@block
def mod_m_counter_visual_test(CLOCK_50, reset_n, HEX0, LEDG, M, N):
    count = Signal(intbv(0)[N:0])
    clk_pulse = Signal(bool(0))
    # complete_tick = Signal(bool(0))

    # reduced clock rate
    clock_tick_inst = clock_tick(CLOCK_50, reset_n, clk_pulse, 50000000, 29)

    # reduce clock rate is applied to mod_m_counter
    # mod_m_counter_inst = mod_m_counter(clk_pulse, reset_n, 
                    # complete_tick, count, M, N)
    mod_m_counter_inst = mod_m_counter(clk_pulse, reset_n, None, count, M, N)

    # count is send for conversion
    hex_to_seven_seg_test_inst = hex_to_seven_seg_test(count, HEX0)

    # send count to LEDG
    @always_comb
    def led_logic():
        LEDG.next = count
    return instances() 

def main():
    CLOCK_50 = Signal(bool(0))
    reset_n = Signal(bool(0))
    HEX0 = Signal(intbv(0)[7:0])
    LEDG = Signal(intbv(0)[4:0])
    M = 12
    N = 4

    mod_m_counter_inst = mod_m_counter_visual_test(CLOCK_50, reset_n, HEX0, LEDG, M, N)
    mod_m_counter_inst.convert(hdl="Verilog", initial_values=True)

if __name__ == '__main__':
    main()
