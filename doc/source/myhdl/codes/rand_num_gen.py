# rand_num_gen.py
# result is same as 'rand_num_generator.v' in the Verilog tutorial

from myhdl import *

# from mod_m_counter import mod_m_counter
from clock_tick import clock_tick
from hex_to_seven_seg import hex_to_seven_seg

@block
def rand_num_gen(clk, reset_n, q):
    N = 3
    # change the value of feedback_value in feedback_logic according to
    # the value of N
    r_reg = Signal(intbv(0)[N+1:0])
    r_next = Signal(intbv(0)[N+1:0])

    feedback_value = Signal(bool(0))

    @always(clk.posedge, reset_n.negedge)
    def current_state_logic():
        if reset_n == 0 :
            r_reg.next = 1
        else :
            r_reg.next = r_next

    @always_comb
    def feedback_logic():
        # uncomment correct line based on the value of N

        feedback_value.next = r_reg[3] ^ r_reg[2] ^ r_reg[0] # N = 3
        # feedback_value.next = r_reg[4] ^ r_reg[3] ^ r_reg[0] # N = 4
        # feedback_value.next = r_reg[5] ^ r_reg[3] ^ r_reg[0] # N = 5
        # feedback_value.next = r_reg[9] ^ r_reg[5] ^ r_reg[0] # N = 9

    @always_comb
    def next_state_logic(): #
        r_next.next = concat(feedback_value, r_reg[N+1:1])

        # # use above or uncomment below two lines
        # r_next[N].next = feedback_value
        # r_next[N:0].next = r_reg[N+1:1]


    @always_comb
    def assign_output():
        q.next = r_reg

    return current_state_logic, feedback_logic, \
        assign_output, next_state_logic

@block
def rand_num_gen_top(CLOCK_50, reset_n, LEDG, HEX0):
    clk_pulse = Signal(bool(0))
    count = Signal(intbv(0)[4:0])

    # reduced clock rate
    clock_tick_inst = clock_tick(CLOCK_50, reset_n,
            clk_pulse, 50000000, 29)

    # rand_num_gen with new port names
    rand_num_gen_verilog = rand_num_gen(clk=clk_pulse,
            reset_n=reset_n, q=count)

    # below will not work for N > 3 as seven-segment can display upto F
    hex_to_seven_seg_test_inst = hex_to_seven_seg(count, HEX0)

    # send count to LEDG
    @always_comb
    def led_logic():
        LEDG.next = count

    return rand_num_gen_verilog, clock_tick_inst, \
                hex_to_seven_seg_test_inst, led_logic


def main():
    N = 3  # set the value of same as rand_num_gen
    clk = Signal(bool(0))
    reset_n = Signal(bool(0))
    q = Signal(intbv(0)[N+1:0])

    # HEX0 will work for N = 3 only, as seven segment can display till F only
    HEX0 = Signal(intbv(0)[7:0])

    # rand_num_gen for simulation
    rand_num_gen_verilog = rand_num_gen(clk, reset_n, q)
    rand_num_gen_verilog.convert(hdl="Verilog", initial_values=True)

    # rand_num_gen for visual testing
    rand_num_gen_top_verilog = rand_num_gen_top(clk, reset_n, q, HEX0)
    rand_num_gen_top_verilog.convert(hdl="Verilog", initial_values=True)

if __name__ == '__main__':
    main()
