# rom_seven_segment.py
# the result is same as "ROM_seven_segment.v" in Verilog tutorial
from myhdl import *

@block
def rom_seven_segment(addr, dout):
    # address width = 4
    # data width = 7
    data = (
                0b1000000,
                0b1111001,
                0b0100100,
                0b0110000,
                0b0011001,
                0b0010010,
                0b0000010,
                0b1111000,
                0b0000000,
                0b0010000,
                0b0001000,
                0b0000011,
                0b1000110,
                0b0100001,
                0b0000110,
                0b0001110
            )

    @always_comb
    def read_logic():
        dout.next = data[int(addr)]

    return read_logic

# top level design with updated port names
@block
def rom_ss_visual_test(SW, HEX0, LEDG):
    data = Signal(intbv(0)[7:0])
    rom_seven_segment_inst = rom_seven_segment(SW, data)

    @always_comb
    def display_logic():
        HEX0.next = data
        LEDG.next = data

    return display_logic, rom_seven_segment_inst

# verilog conversion
def main():
    SW = Signal(intbv(0)[4:0])
    LEDG = Signal(intbv(0)[7:0])
    HEX0 = Signal(intbv(0)[7:0])

    rom_seven_segment_verilog = rom_ss_visual_test(SW, HEX0, LEDG)
    rom_seven_segment_verilog.convert(hdl="Verilog", initial_values=True)

if __name__ == '__main__' :
    main()
