# sequence_detector.py
# result is same as the listing "sequence_detector.v" in verilog tutorial

from myhdl import *

@block
def sequence_detector(clk, reset_n, x, z_mealy_glitch, z_moore_glitch,
        z_mealy_glitch_free, z_moore_glitch_free):


    # mealy states
    mealy_states = enum('zero_mealy', 'one_mealy', 'two_mealy', 'three_mealy')
    state_mealy_reg = Signal(mealy_states.zero_mealy)
    state_mealy_next = Signal(mealy_states.zero_mealy)

    # moore states
    moore_states = enum('zero_moore', 'one_moore', 'two_moore', 'three_moore')
    state_moore_reg = Signal(moore_states.zero_moore)
    state_moore_next = Signal(moore_states.zero_moore)

    z_moore = Signal(bool(0))
    z_mealy = Signal(bool(0))

    @always(clk.posedge, reset_n.negedge)
    def current_state_logic():
        if reset_n == 0 :
            state_mealy_reg.next = mealy_states.zero_mealy
            state_moore_reg.next = moore_states.zero_moore
        else :
            state_mealy_reg.next = state_mealy_next
            state_moore_reg.next = state_moore_next

    # next state mealy logic
    @always_comb
    def next_state_logic_mealy():
        z_mealy.next = 0
        state_mealy_next.next = state_mealy_reg
        if state_mealy_reg == mealy_states.zero_mealy:
            if x == 1 :
                state_mealy_next.next = mealy_states.one_mealy
        elif state_mealy_reg == mealy_states.one_mealy :
            if x == 1 :
                state_mealy_next.next = mealy_states.two_mealy
            else :
                state_mealy_next.next = mealy_states.zero_mealy
        elif state_mealy_reg == mealy_states.two_mealy :
            state_mealy_next.next = mealy_states.zero_mealy
            if x == 0 :
                z_mealy.next = 1
            else :
                state_mealy_next.next = mealy_states.two_mealy

    # next state moore logic
    @always_comb
    def next_state_logic_moore():
        z_moore.next = 0
        state_moore_next.next = state_moore_reg
        if state_moore_reg == moore_states.zero_moore:
            if x == 1 :
                state_moore_next.next = moore_states.one_moore
        elif state_moore_reg == moore_states.one_moore :
            if x == 1 :
                state_moore_next.next = moore_states.two_moore
            else :
                state_moore_next.next = moore_states.zero_moore
        elif state_moore_reg == moore_states.two_moore :
            if x == 0 :
                state_moore_next.next = moore_states.three_moore
            else :
                state_moore_next.next = moore_states.two_moore
        elif state_moore_reg == moore_states.three_moore :
            z_moore.next = 1
            if x == 1 :
                state_moore_next.next = moore_states.zero_moore
            else :
                state_moore_next.next = moore_states.one_moore


    @always_comb
    def assign_outputs():
        z_mealy_glitch.next = z_mealy
        z_moore_glitch.next = z_moore

    @always(clk.posedge, reset_n.negedge)
    def glitch_free_output():
        if reset_n == 1 :
            z_mealy_glitch_free.next = 0
            z_moore_glitch_free.next = 0
        else :
            z_mealy_glitch_free.next = z_mealy
            z_moore_glitch_free.next = z_moore


    return current_state_logic, next_state_logic_mealy, \
            next_state_logic_moore, assign_outputs, glitch_free_output

def main():
    clk = Signal(bool(0))
    reset_n = Signal(bool(0))
    x = Signal(bool(0))
    z_mealy_glitch = Signal(bool(0))
    z_moore_glitch = Signal(bool(0))
    z_mealy_glitch_free = Signal(bool(0))
    z_moore_glitch_free = Signal(bool(0))

    sequence_detector_verilog = sequence_detector(clk, reset_n, x,
            z_mealy_glitch, z_moore_glitch,
            z_mealy_glitch_free, z_moore_glitch_free)

    sequence_detector_verilog.convert(hdl="Verilog", initial_values=True)

if __name__ == '__main__':
    main()
