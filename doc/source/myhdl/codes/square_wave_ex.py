# square_wave_ex.py
# results are same as Listing "square_wave_ex.v" in Verilog tutorial

from myhdl import *

@block
def square_wave_ex(clk, reset_n, s_wave, on_time, off_time, N):
    states = enum('on_state', 'off_state')
    state_reg = Signal(states.on_state)
    state_next = Signal(states.on_state)
    t = Signal(intbv(0)[N:0])

    @always(clk.posedge, reset_n.negedge)
    def current_state_logic():
        if reset_n == 0 :
            state_reg.next = states.off_state
        else :
            state_reg.next = state_next

    @always(clk.posedge, reset_n.negedge)
    def timer_logic():
        if state_reg != state_next :
            t.next = 0
        else :
            t.next = t + 1

    @always_comb
    def next_state_logic():
        if state_reg == states.off_state :
            s_wave.next = 0
            if t == off_time - 1 :
                state_next.next = states.on_state
            else :
                state_next.next = states.off_state
        if state_reg == states.on_state :
            s_wave.next = 1
            if t == on_time - 1 :
                state_next.next = states.off_state
            else :
                state_next.next = states.on_state

    return current_state_logic, timer_logic, next_state_logic

def main():
    N = 4
    on_time = 5
    off_time = 3
    clk = Signal(bool(0))
    reset_n = Signal(bool(0))
    s_wave = Signal(bool(0))

    square_wave_verilog = square_wave_ex(clk, reset_n,
            s_wave, on_time, off_time, N)

    square_wave_verilog.convert(hdl="Verilog", initial_values=True)

if __name__ == '__main__':
    main()
