.. _`Testbench`:

Testbench
=========


In this chapter, we write the testbench for the :numref:`py_mod_m_counter`. This testbench contains several features of MyHDL which are enough to start writing the testbenches. Also, conversion of MyHDL testbench to HDL testbench is discussed. 


.. _`tb_mod_m_counter`:

Mod-m counter 
-------------

* In this section, we will write a testbench for Mod m coutner. For this, the :numref:`py_mod_m_counter` is modified as below, 

    .. code-block:: python
        :linenos:
        :emphasize-lines: 5, 49-73, 88-91

        # mod_m_counter.py

        from myhdl import *

        period = 20 # clk frequency = 50 MHz

        @block
        def mod_m_counter(clk, reset_n, complete_tick, count, M, N):
            """ M = max count
                N = minimum bits required to represent M
            """

            count_reg = Signal(intbv(0)[N:0])
            count_next = Signal(intbv(0)[N:0])

            @always(clk.posedge, reset_n.negedge)
            def logic_reg():
                if reset_n == 0 :
                    count_reg.next = 0
                else :
                    count_reg.next = count_next

            @always_comb
            def logic_next():
                if count_reg == M-1 :
                    count_next.next = 0
                else :
                    count_next.next = count_reg + 1


            # optional complete_tick
            if complete_tick is not None:
                @always_comb
                def complete_tick_logic():
                    if count_reg == M-1 :
                        complete_tick.next = 1
                    else :
                        complete_tick.next = 0

            # optional count
            if count is not None:
                @always_comb
                def out_val():
                    count.next = count_reg

            return instances()  # return all instances


        # testbench
        @block 
        def mod_m_counter_tb():
            N = 3
            M = 5

            clk = Signal(bool(0))
            reset_n = Signal(bool(0))
            complete_tick = Signal(bool(0))
            count = Signal(intbv(0)[N:0])

            mod_m_counter_inst = mod_m_counter(clk, reset_n, complete_tick, count, M, N)

            # int is required 
            @always(delay(int(period/2)))
            def clk_signal():
                clk.next = not clk

            @instance  # reset signal
            def reset_signal():
                reset_n.next = 0
                yield delay(period) 
                reset_n.next = 1

            return instances()


        def main():
            N = 3
            M = 5

            clk = Signal(bool(0))
            reset_n = Signal(bool(0))
            complete_tick = Signal(bool(0))
            count = Signal(intbv(0)[N:0])

            mod_m_counter_v = mod_m_counter(clk, reset_n, complete_tick, count, M, N)
            mod_m_counter_v.convert(hdl="Verilog", initial_values=True)

            # test bench
            tb = mod_m_counter_tb()
            tb.config_sim(trace=True)
            tb.run_sim(15*period) # run for 15 clock cycle

        if __name__ == '__main__':
            main()

* If we run the above code, then a `vcd` file, i.e. mod_m_counter_tb.vcd', will be generated, which can be open in `gtkwave <http://linux-software-list.readthedocs.io/en/latest/readme.html#gtk-wave>`_. Following is the results saved in the .vcd file, 

    .. _`mod_m_counter_tb`:    

    .. figure:: figures/mod_m_counter_tb.png

       Simulation results of mod_m_counter.py


Saving data to file
-------------------

* Sometimes we want to save the testbench-data in the file for further analysis of the data. In the below listing, the code in the above :numref:`tb_mod_m_counter` is slightly modified to save the data in the file and printed on the screen. 

    .. important:: 

        * Note that, the signals at Lines 55-61 are declared as 'intbv' instead of 'bool', because the 'bool' signal is saved as 'True/False' which may not be desirable for the analysis of the data. 
        * Lines 79-93 prints the data on screen and save the data in the file 'mod_m_counter.csv'. 
    
    
    .. code-block:: python
        :linenos:
        :emphasize-lines: 55-61, 79-93

        # mod_m_counter.py

        from myhdl import *

        period = 20 # clk frequency = 50 MHz

        @block
        def mod_m_counter(clk, reset_n, complete_tick, count, M, N):
            """ M = max count
                N = minimum bits required to represent M
            """

            count_reg = Signal(intbv(0)[N:0])
            count_next = Signal(intbv(0)[N:0])

            @always(clk.posedge, reset_n.negedge)
            def logic_reg():
                if reset_n == 0 :
                    count_reg.next = 0
                else :
                    count_reg.next = count_next

            @always_comb
            def logic_next():
                if count_reg == M-1 :
                    count_next.next = 0
                else :
                    count_next.next = count_reg + 1


            # optional complete_tick
            if complete_tick is not None:
                @always_comb
                def complete_tick_logic():
                    if count_reg == M-1 :
                        complete_tick.next = 1
                    else :
                        complete_tick.next = 0

            # optional count
            if count is not None:
                @always_comb
                def out_val():
                    count.next = count_reg

            return instances()  # return all instances


        # testbench
        @block
        def mod_m_counter_tb():
            N = 3
            M = 5

            # 'intbv' is used instead of 'bool' as 'false' will be displayed
            # in the saved-data for 'bool'. 
            # clk = Signal(bool(0))
            clk = Signal(intbv(0)[1:0]) 
            reset_n = Signal(intbv(0)[1:0])
            complete_tick = Signal(intbv(0)[1:0])
            count = Signal(intbv(0)[N:0])

            mod_m_counter_inst = mod_m_counter(clk, reset_n, complete_tick, count, M, N)

            
            # int is required
            @always(delay(int(period/2)))
            def clk_signal():
                clk.next = not clk

                
            @instance  # reset signal
            def reset_signal():
                reset_n.next = 0
                yield delay(period)
                reset_n.next = 1
                
            
            # print simulation data on screen and file
            file_data = open("mod_m_counter.csv", 'w') # file for saving data
            # print header on screen
            print("{0},{1},{2}".format("reset_n", "complete_tick", "count"))
            # print header to file
            print("{0},{1},{2}".format("reset_n", "complete_tick", "count")
                        , file=file_data)
            # print data on each clock
            @always(clk.posedge)
            def print_data():
                # print on screen
                print("{0}, {1}, {2}".format(reset_n, complete_tick, count))
                # print in file
                print("{0}, {1}, {2}".format(reset_n, complete_tick, count)
                        , file=file_data) 


            return instances()


        def main():
            N = 3
            M = 5

            clk = Signal(bool(0))
            reset_n = Signal(bool(0))
            complete_tick = Signal(bool(0))
            count = Signal(intbv(0)[N:0])

            mod_m_counter_v = mod_m_counter(clk, reset_n, complete_tick, count, M, N)
            mod_m_counter_v.convert(hdl="Verilog", initial_values=True)

            # test bench
            tb = mod_m_counter_tb()
            tb.config_sim(trace=True)
            tb.run_sim(15*period) # run for 15 clock cycle
            

        if __name__ == '__main__':
            main()



* Following is the data which is saved in the file 'mod_m_counter.csv'

    .. code-block:: shell
    
        reset_n,complete_tick,count
        0, 0, 0
        1, 0, 0
        1, 0, 1
        1, 0, 2
        1, 0, 3
        1, 1, 4
        1, 0, 0
        1, 0, 1
        1, 0, 2
        1, 0, 3
        1, 1, 4
        1, 0, 0
        1, 0, 1
        1, 0, 2
        1, 0, 3

Conversion : MyHDL testbench to HDL testbench
---------------------------------------------

* Some changes are required in above listing to convert the MyHDL testbench into HDL testbench, as shown below. Execute the below code to generate the testbench. 
  
    .. note:: 

        * '\@instance' decorator is used with "def clk_signal():", as 'delay' argument is not supported for testbench conversion (Lines 66-70).
        * '.format' option of Python3-print statement is not supported by MyHDL (Lines 91-99). 
        * The conversion statement (Line 119) is placed above the simulation statements (Lines 122-123).
          

    .. code-block:: python
        :linenos:
        :emphasize-lines: 66-70, 87-99, 118-121
    
        # mod_m_counter.py

        from myhdl import *

        period = 20 # clk frequency = 50 MHz

        @block
        def mod_m_counter(clk, reset_n, complete_tick, count, M, N):
            """ M = max count
                N = minimum bits required to represent M
            """

            count_reg = Signal(intbv(0)[N:0])
            count_next = Signal(intbv(0)[N:0])

            @always(clk.posedge, reset_n.negedge)
            def logic_reg():
                if reset_n == 0 :
                    count_reg.next = 0
                else :
                    count_reg.next = count_next

            @always_comb
            def logic_next():
                if count_reg == M-1 :
                    count_next.next = 0
                else :
                    count_next.next = count_reg + 1


            # optional complete_tick
            if complete_tick is not None:
                @always_comb
                def complete_tick_logic():
                    if count_reg == M-1 :
                        complete_tick.next = 1
                    else :
                        complete_tick.next = 0

            # optional count
            if count is not None:
                @always_comb
                def out_val():
                    count.next = count_reg

            return instances()  # return all instances


        # testbench
        @block
        def mod_m_counter_tb():
            N = 3
            M = 5

            # 'intbv' is used instead of 'bool' as 'false' will be displayed
            # in the saved-data for 'bool'.
            # clk = Signal(bool(0))
            clk = Signal(intbv(0)[1:0])
            reset_n = Signal(intbv(0)[1:0])
            complete_tick = Signal(intbv(0)[1:0])
            count = Signal(intbv(0)[N:0])

            mod_m_counter_inst = mod_m_counter(clk, reset_n, complete_tick, count, M, N)


            @instance
            def clk_signal():
                while True:
                    clk.next = not clk
                    yield delay(period//2)


            @instance  # reset signal
            def reset_signal():
                reset_n.next = 0
                yield delay(period)
                reset_n.next = 1


            # print simulation data on screen and file
            file_data = open("mod_m_counter.csv", 'w') # file for saving data
            # # print header on screen
            print("{0},{1},{2}".format("reset_n", "complete_tick", "count"))
            # # print header to file
            print("{0},{1},{2}".format("reset_n", "complete_tick", "count"),
                       file=file_data)
            # print data on each clock
            @always(clk.posedge)
            def print_data():
                # print on screen
                # print.format is not supported in MyHDL 1.0
                # print("{0}, {1}, {2}".format(reset_n, complete_tick, count))
                print(reset_n, ",", complete_tick, ",",  count, sep='')

                # print in file
                # print.format is not supported in MyHDL 1.0
                # print("{0}, {1}, {2}".format(reset_n, complete_tick, count)
                        # , file=file_data)
                print(reset_n, ",", complete_tick, ",",  count, sep='', file=file_data)


            return instances()


        def main():
            N = 3
            M = 5

            clk = Signal(bool(0))
            reset_n = Signal(bool(0))
            complete_tick = Signal(bool(0))
            count = Signal(intbv(0)[N:0])

            mod_m_counter_v = mod_m_counter(clk, reset_n, complete_tick, count, M, N)
            mod_m_counter_v.convert(hdl="Verilog", initial_values=True)

            # test bench
            tb = mod_m_counter_tb()
            tb.convert(hdl="Verilog", initial_values=True)
            # keep following lines below the 'tb.convert' line
            # otherwise error will be reported
            tb.config_sim(trace=True)
            tb.run_sim(15*period) # run for 15 clock cycle


        if __name__ == '__main__':
            main()

* Following is the generated Verilog code for the testbench, 
  
    .. note:: 

        In the above listing, the Python code prints the data on screen and save in the file. But in the resultant Verilog code, the data will not be saved in the file (but will be printed on the screen).
        
  
    .. code-block:: verilog
    
        // File: mod_m_counter_tb.v
        // Generated by MyHDL 1.0dev
        // Date: Mon Oct 16 11:44:15 2017


        `timescale 1ns/10ps

        module mod_m_counter_tb (

        );

        reg [0:0] reset_n = 0;
        wire [2:0] count;
        reg [0:0] complete_tick = 0;
        reg [0:0] clk = 0;
        reg [2:0] mod_m_counter_1_count_reg = 0;
        reg [2:0] mod_m_counter_1_count_next = 0;

        always @(posedge clk, negedge reset_n) begin: MOD_M_COUNTER_TB_MOD_M_COUNTER_1_LOGIC_REG
            if ((reset_n == 0)) begin
                mod_m_counter_1_count_reg <= 0;
            end
            else begin
                mod_m_counter_1_count_reg <= mod_m_counter_1_count_next;
            end
        end


        always @(mod_m_counter_1_count_reg) begin: MOD_M_COUNTER_TB_MOD_M_COUNTER_1_LOGIC_NEXT
            if (($signed({1'b0, mod_m_counter_1_count_reg}) == (5 - 1))) begin
                mod_m_counter_1_count_next = 0;
            end
            else begin
                mod_m_counter_1_count_next = (mod_m_counter_1_count_reg + 1);
            end
        end


        always @(mod_m_counter_1_count_reg) begin: MOD_M_COUNTER_TB_MOD_M_COUNTER_1_COMPLETE_TICK_LOGIC
            if (($signed({1'b0, mod_m_counter_1_count_reg}) == (5 - 1))) begin
                complete_tick = 1;
            end
            else begin
                complete_tick = 0;
            end
        end

        assign count = mod_m_counter_1_count_reg;

        initial begin: MOD_M_COUNTER_TB_CLK_SIGNAL
            while (1'b1) begin
                clk <= (!clk);
                # (20 / 2);
            end
        end

        initial begin: MOD_M_COUNTER_TB_RESET_SIGNAL
            reset_n <= 0;
            # 20;
            reset_n <= 1;
        end

        always @(posedge clk) begin: MOD_M_COUNTER_TB_PRINT_DATA
            $write("%h", reset_n);
            $write(" ");
            $write(",");
            $write(" ");
            $write("%h", complete_tick);
            $write(" ");
            $write(",");
            $write(" ");
            $write("%h", count);
            $write("\n");
            $write("%h", reset_n);
            $write(" ");
            $write(",");
            $write(" ");
            $write("%h", complete_tick);
            $write(" ");
            $write(",");
            $write(" ");
            $write("%h", count);
            $write("\n");
        end

        endmodule


* Below is the simulation results for the above testbench, which is same as :numref:`mod_m_counter_tb` generated by MyHDL testbench.
  
    .. _`mod_m_counter_verilog_tb`:    
    
    .. figure:: figures/mod_m_counter_verilog_tb.png

       Simulation results of mod_m_counter.v

Conclusion
----------

In this chapter, we wrote the testbench for the Mod-m counter. First we wrote a testbench which saves the data in the .vcd file. Next, we learn the method to save the data in the file. Lastly, we modified the MyHDL testbench to convert it into HDL testbench. 